import React, { Component } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import Footer from "./include/Footer";
import Loader from 'react-loader-spinner'
class Blog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            posts: [],
            loadingCategory: true,
            loadingPost: true
        };
    }


    componentDidMount() {
        const fetchCategory = fetch('https://blog.ryanadhitama.com/api/category.php');
        fetchCategory
            .then(response => response.json())
            .then(data => {
                this.setState({
                    categories: data,
                    loadingCategory: false
                })
            })

        const fetchPost = fetch('https://blog.ryanadhitama.com/api/blog.php');
        fetchPost
            .then(response => response.json())
            .then(data => {
                this.setState({
                    posts: data,
                    loadingPost: false
                })
            })
    }

    render() {
        let LoadingCategory;
        if (this.state.loadingCategory) {
            LoadingCategory = <Loader
                className="loadCategory"
                type="ThreeDots"
                color="#999"
                height={50}
                width={50}
                timeout={3000} //3 secs
            />;
        } else {
            LoadingCategory = '';
        }
        let LoadingPost;
        if (this.state.loadingPost) {
            LoadingPost = <Loader
                className="loadPost"
                type="ThreeDots"
                color="#999"
                height={50}
                width={50}
                timeout={3000} //3 secs
            />;
        } else {
            LoadingPost = '';
        }
        return (
            <div className="row">

                <div className="col col-8">
                    <div className="card">
                        <div className="card-header">
                            List Blog
					</div>
                        <div className="card-body">

                            {
                                this.state.posts.map((post, index) => {
                                    return (
                                        <div className="post" key={index}>
                                            <h4>{post.name}</h4>
                                            <small>{post.date}</small>
                                            <p>{post.description} ...</p>
                                            <Link to={`/blog/${post.slug}`}>Read More</Link>

                                        </div>
                                    );
                                })
                            }
                            {LoadingPost}

                            <br />
                            <a href="https://blog.ryanadhitama.com">Visit Blog</a>
                        </div>
                    </div>
                </div>
                <div className="col col-4">
                    <div className="card">
                        <div className="card-header">
                            Category
					</div>
                        <div className="card-body">
                            <ul className="listNone">
                                {
                                    this.state.categories.map((category, index) => {
                                        return (<li key={index}><a href={category.url} target="_blank" rel="noopener noreferrer">{category.name}</a></li>);
                                    })
                                }

                            </ul>
                            {LoadingCategory}
                        </div>
                    </div>
                </div>
                <Footer />
                <Router></Router>
            </div >
        )
    }
}

export default Blog;