import React, { Component } from "react";
import Footer from "./include/Footer";
import Marketbiz from './img/marketbiz.jpg';
import Stikom from './img/stikom-logo.png';
import Skensa from './img/skensa.png';

class Home extends Component {
    render() {
        return (
            <div className="row">

                <div className="col col-4">
                    <div className="card">
                        <div className="card-header">
                            Personal Information
					</div>
                        <div className="card-body">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Name
								</td>
                                        <td>
                                            : Ryan Adhitama Putra
								</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Phone
								</td>
                                        <td>
                                            : 081558737080
								</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Location
								</td>
                                        <td>
                                            : Denpasar, Bali
								</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col col-8">
                    <div className="card">
                        <div className="card-header">
                            About
					</div>
                        <div className="card-body">
                            Hi! My name is Ryan Adhitama Putra. You can call me as 'ryan'.

					</div>
                    </div>

                    <div className="card">
                        <div className="card-header">
                            Experience
					</div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col col-4 text-center-width">
                                    <br />
                                    <img alt="marketbiz" className="iconGroup" src={Marketbiz} />

                                </div>
                                <div className="col col-8 border-bottom">

                                    <b>Web Developer</b> <br /><br />
                                    <p>PT Marketbiz Jendela Digital</p>
                                    <small>Jun 2018 - Now</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header">
                            Education
					</div>
                        <div className="card-body">
                            <div className="row" >
                                <div className="col col-4 text-center-width">
                                    <br />
                                    <img alt="Stikom" className="iconGroup" src={Stikom} />

                                </div>
                                <div className="col col-8 border-bottom">
                                    <b>ITB STIKOM Bali</b> <br /><br />
                                    <p>Bachelor's Degree, Information System</p>
                                    <small>2019 - 2023</small>
                                </div>
                            </div>

                            <div className="row" >
                                <div className="col col-4 text-center-width">
                                    <br />
                                    <img alt="Skensa" className="iconGroup" src={Skensa} />

                                </div>
                                <div className="col col-8 border-bottom">
                                    <b>SMKN 1 Denpasar</b> <br /><br />
                                    <p>Software Engineer</p>
                                    <small>2015 - 2018</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Home;