import React, { Component } from "react";
import Footer from "./include/Footer";
import Loader from 'react-loader-spinner'
import { BrowserRouter as Router, Link } from "react-router-dom";
import Parser from 'html-react-parser';

class BlogDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            slug: this.props.match.params.slug,
            title: "Loading",
            content: "",
            other: [],
            loadingCategory: "",
            loadingPost: ""
        };
    }

    componentWillUnmount() {
        this.setState({
            loadingCategory: <Loader
                className="loadCategory"
                type="ThreeDots"
                color="#999"
                height={50}
                width={50}
                timeout={3000} //3 secs
            />, loadingPost: <Loader
                className="loadPost"
                type="ThreeDots"
                color="#999"
                height={50}
                width={50}
                timeout={3000} //3 secs
            />,

        })
    }

    componentDidMount() {

        let slug = this.props.match.params.slug;

        this.setState({
            slug: slug
        })

        const fetchPost = fetch('https://blog.ryanadhitama.com/api/blogDetail.php?slug=' + this.state.slug);

        fetchPost
            .then(response => response.json())
            .then(data => {
                this.setState({
                    title: data.title,
                    content: data.content,
                    loadingPost: ""
                })
            })


        const fetchOther = fetch('https://blog.ryanadhitama.com/api/blog.php');

        fetchOther
            .then(response => response.json())
            .then(data => {
                this.setState({
                    other: data,
                    loadingCategory: ""
                })
            })


    }


    render() {

        return (
            <div className="row">
                <div className="col col-8">
                    <div className="card">
                        <div className="card-header">
                            {this.state.title}
                        </div>
                        <div className="card-body">
                            {Parser(this.state.content)}

                            {this.state.loadingPost}
                        </div>
                    </div>
                </div>
                <div className="col col-4">
                    <div className="card">
                        <div className="card-header">
                            Other
					</div>
                        <div className="card-body">
                            <ul className="listNone">
                                {
                                    this.state.other.map((post, index) => {
                                        return (<li key={index}><Link to={{ pathname: `/blog/${post.slug}` }} replace>{post.name}</Link></li>);
                                    })
                                }

                            </ul>
                            {this.state.loadingCategory}
                        </div>
                    </div>
                </div>

                <Footer />
                <Router></Router>
            </div>
        )
    }
}

export default BlogDetail;