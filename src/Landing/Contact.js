import React, { Component } from "react";
import Footer from "./include/Footer";
import Social from "./img/social.png"

class Podcast extends Component {
    render() {
        return (
            <div className="row">
                <div className="col col-4">
                    <div className="card">
                        <div className="card-header">
                            Social Media
					</div>
                        <div className="card-body">
                            <img src={Social} alt={Social} />
                        </div>
                    </div>
                </div>
                <div className="col col-8">
                    <div className="card">
                        <div className="card-header">
                            Message
					</div>
                        <div className="card-body">
                            <p>
                                Say <b>Hello</b> <br />
                                I also open collaboration and partnership. <br />
                                <small>ryanadhitama2@gmail.com</small>
                            </p>
                            <br />
                            <form>
                                <div className="row">
                                    <div className="col col-6">
                                        <div className="form-group">
                                            <label><b>Name</b></label>
                                            <input type="text" placeholder="Enter your name" className="form-control" name="" />
                                        </div>
                                    </div>
                                    <div className="col col-6">
                                        <div className="form-group">
                                            <label><b>Email</b></label>
                                            <input type="email" placeholder="Enter your email" className="form-control" name="" />
                                        </div>
                                    </div>
                                    <div className="col col-12">
                                        <div className="form-group">
                                            <label><b>Message</b></label>
                                            <textarea className="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div className="col col-12">
                                        <div className="form-group">
                                            <button className="btn btn-submit">
                                                Under Maintenance
										</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                < Footer />
            </div>
        )
    }
}

export default Podcast;