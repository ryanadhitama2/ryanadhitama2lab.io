import React, { Component } from "react";
import { Route, NavLink, BrowserRouter, Router } from "react-router-dom";

import Home from "./Home";
import Blog from "./Blog";
import BlogDetail from "./BlogDetail";
import Podcast from "./Podcast";
import Contact from "./Contact";
import Face from "./img/face.png"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

class Main extends Component {

    render() {
        const checkActive = (match, location) => {
            //some additional logic to verify you are in the home URI
            if (!location) return false;
            const { pathname } = location;
            console.log(pathname);
            return pathname === "/";
        }
        return (
            <div>
                <header>
                    <img src={Face} className="header-face" alt={Face} />
                </header>
                <div className="container">
                    <div className="intro">
                        <h2>Ryan Adhitama Putra</h2>
                        <p>Software Engineer</p>


                        <div>

                            <BrowserRouter>
                                <ul className="nav">
                                    <li><NavLink to="/" isActive={checkActive}>Home</NavLink></li>
                                    <li><NavLink to="/blogs">Blog</NavLink></li>
                                    <li><NavLink to="/podcast">Podcast</NavLink></li>
                                    <li><NavLink to="/contact">Contact</NavLink></li>
                                </ul>
                                <div >
                                    <Route exact path="/" component={Home} />
                                    <Route exact path="/blog/:slug" component={BlogDetail} />
                                    <Route path="/blogs" component={Blog} />
                                    <Route path="/podcast" component={Podcast} />
                                    <Route path="/contact" component={Contact} />


                                </div>
                            </BrowserRouter>
                            <Router></Router>
                        </div>

                    </div>
                </div>
            </div >
        )
    }

}

export default Main;