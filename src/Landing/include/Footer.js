import React, { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <div className="col col-12">
                <br />
                <hr />
                <br />
                <div className="text-center">
                    <small>Copyright &copy; 2019</small>
                </div>
                <br />
            </div>
        )
    }
}

export default Footer;