import React, { Component } from "react";
import Footer from "./include/Footer";
import Loader from 'react-loader-spinner';

class Podcast extends Component {
    constructor(props) {
        super(props);

        this.state = {
            podcast: [],
            loaderPodcast: <Loader
                className="loadPost"
                type="ThreeDots"
                color="#999"
                height={50}
                width={50}
                timeout={3000} //3 secs
            />
        };
    }
    componentDidMount() {

        const fetchPost = fetch('https://blog.ryanadhitama.com/api/podcast.php');

        fetchPost
            .then(response => response.json())
            .then(data => {
                this.setState({
                    podcast: data
                })
            })
    }
    hideSpinner = () => {
        this.setState({
            loaderPodcast: false
        });
    };
    render() {
        return (
            <div className="row">
                <div className="col col-12">

                    {
                        this.state.podcast.map((row, index) => {
                            return (
                                <iframe title={row.url} onload={this.hideSpinner} key={index} src={row.url} width="95%" frameborder="0" scrolling="no"></iframe>
                            )
                        })
                    }

                    {this.state.loaderPodcast}

                    <br />
                </div>
                <Footer />
            </div>
        )
    }
}

export default Podcast;