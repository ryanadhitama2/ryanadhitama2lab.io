import React from 'react';
import ReactDOM from 'react-dom';
import './Landing/css/style.css';
import Main from './Landing/Main';
// import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Main />, document.getElementById('root'));
